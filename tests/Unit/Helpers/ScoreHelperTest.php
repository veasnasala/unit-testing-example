<?php

namespace Tests\Unit\Helpers;

use PHPUnit\Framework\TestCase;
use App\Helpers\ScoreHelper;

class ScoreHelperTest extends TestCase
{
    protected $score_helper;

    public function setUp()
    {
        $this->score_helper = new ScoreHelper();
    }

    public function test_getValueByPercentageAndMaxValue()
    {
        $value = $this->score_helper->getValueByPercentageAndMaxValue(33.3333, 150);
        $this->assertEquals(50, $value);
    }

    public function test_calculateAverageScore()
    {
        $average = $this->score_helper->calculateAverageScore(270, 3);
        $this->assertEquals(90, $average);
    }

}