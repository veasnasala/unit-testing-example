<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 2019-05-10
 * Time: 09:02
 */

namespace Tests\Unit\Helpers;

use PHPUnit\Framework\TestCase;
use App\Helpers\ScoreHelper;
use App\Helpers\UtilityHelper;

class ScoreHelperTest extends TestCase
{

    public function test_getPercentageByValueAndMaxValue()
    {
        $utilityHelper = new UtilityHelper();
        $result = $utilityHelper->getPercentageByValueAndMaxValue(20, 100);

        $this->assertEquals(20, $result);
    }

    public function test_getValueByPercentageAndMaxValue()
    {
        $utilityHelper = new UtilityHelper();
        $result = $utilityHelper->getValueByPercentageAndMaxValue(50, 200);
        $this->assertEquals(100, $result);
    }
}