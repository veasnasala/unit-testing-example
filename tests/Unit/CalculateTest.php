<?php

namespace Tests\Unit;

use App\Calculate;
use PHPUnit\Framework\TestCase;

class CalculateTest extends TestCase
{
    public function setUp()
    {
        $this->calculate = new Calculate();
    }

    public function test_areaOfSquare_WhenCalledWithLength2_Return4()
    {
        $length = 2;
        $response = $this->calculate->areaOfSquare($length);
        $this->assertTrue(is_int($response));
        $this->assertEquals(4, $response);
    }


    public function test_areaOfSquare_WhenCalledWithLength6_Return36()
    {
        $length = 6;
        $response = $this->calculate->areaOfSquare($length);
        $this->assertTrue(is_int($response));
        $this->assertEquals(36, $response);
    }

    public function test_areaOfSquare_WhenCalledWithoutLength_ThrowAnException()
    {
        $this->expectException('ArgumentCountError');
        $this->expectExceptionMessage('Too few arguments to function');
        $this->calculate->areaOfSquare();
    }
}