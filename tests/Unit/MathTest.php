<?php

namespace Tests\Unit;

use App\Math;
use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Mockery\Adapter\Phpunit\MockeryTestCase;

class MathTest extends MockeryTestCase
{
    use WithFaker;

    public function setUp()
    {
        $this->calculate = m::mock('App\Calculate');
        $this->user = m::mock('App\User');
        $this->math = new Math($this->calculate, $this->user);
        $this->setUpFaker();
    }

    public function test_getArea_WhenCalledWithLength2_Return4()
    {
        // generate first name
        $first_name = $this->faker->firstName;
        $last_name = $this->faker->lastName;
        $gender = $this->faker->randomElement(['M', 'F']);
        $gender_full = 'Mr. ';

        $this->user->shouldReceive('getFirstName')
            ->andReturn($first_name)
            ->once();
        $this->user->shouldReceive('getLastName')
            ->andReturn($last_name)
            ->once();
        $this->user->shouldReceive('getGender')
            ->andReturn($gender)
            ->once();
        $this->user->shouldReceive('getGenderByShortCut')
            ->andReturn($gender_full)
            ->once();
        $response = $this->math->getFullNameByUser();
//        dd($response);
//        $this->assertTrue(is_int($response));
        $this->assertEquals($gender_full . $first_name . ' ' . $last_name, $response);
    }
}
