<?php

namespace Tests\Unit;

use App\Task;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TasksTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_if_order_by_last()
    {
        // Given I have 3 records in the database those are Tasks
        factory('App\Task', 3)->create();
        // And last record one this is tasks
        $last_task = factory('App\Task')->create();
        // When I fetch getByLast
        $tasks = Task::getByLast();
        // Then the response should be in format.
        $this->assertEquals($last_task->id, $tasks->first()->id);
    }
}
