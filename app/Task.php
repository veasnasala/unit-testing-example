<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = [];

    public static function getByLast() {
        return Task::orderBy('id', 'desc')->get();
    }
}
