<?php namespace App\Http\ViewComposers;

use Illuminate\View\View;

class FooComposer
{
    public function compose(View $view)
    {
        $foo = 'foo testing';

        $view->with('foo', $foo);
    }
}