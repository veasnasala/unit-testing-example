<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 2019-05-10
 * Time: 09:02
 */
namespace App\Helpers;
class UtilityHelper {

    public function getPercentageByValueAndMaxValue($value, $max_value) {
        return $percentage = $value/ $max_value * 100 ;
    }

    public function getValueByPercentageAndMaxValue($percentage, $max_value) {
        return $value = ($percentage/ 100) * $max_value;
    }
}