<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 2019-05-08
 * Time: 17:05
 */

namespace App\Helpers;

class ScoreHelper {

    public function getPercentageByValueAndMaxValue($value, $max_value) {
        $percentage = ($value / $max_value ) * 100;
        return number_format($percentage, 4);
    }

    public function getValueByPercentageAndMaxValue($percentage, $max_value) {
        $value = $percentage * $max_value / 100;
        return number_format($value, 4);
    }
    public function calculateAverageScore($total_score, $total_subject){
        $value = $total_score/$total_subject;
        return $value;

    }
}