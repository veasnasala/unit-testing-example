<?php
namespace App;
class Math
{
    /**
     * @var Calculate
     */
    public $calculate;
    protected  $user;
    /**
     * Math constructor.
     *
     * @param Calculate $calculate
     */
    public function __construct(Calculate $calculate, User $user)
    {
        $this->calculate = $calculate;
        $this->user = $user;
    }
    /**
     * Get Area
     *
     * @param $length
     *
     * @return float|int
     */
    public function getArea($length)
    {
        return $this->calculate->areaOfSquare($length) / 2;
    }

//    public  function  getGenderByShortCut($gender) {
//        if($gender === 'M') {
//           return 'Mr. ';
//        }else {
//            return 'Mrs. ';
//        }
//    }
    public function getFullNameByUser()
    {
        $user = $this->user;
        $prefix = $user->getGenderByShortCut($user->getGender());
        return $prefix. $user->getFirstName() . ' '. $user->getLastName();
    }
}